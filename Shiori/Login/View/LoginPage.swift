//
//  LoginPage.swift
//  Shiori
//
//  Created by Abilash S on 24/02/24.
//

import SwiftUI

struct LoginPage: View {
    
    @ObservedObject var viewModel: LoginViewModel
    
    @FocusState private var focusedField: LoginViewModel.Field?
    
    var body: some View {
        ZStack {
            Color(.secondaryBackground)
                .ignoresSafeArea(.all)
            
            VStack {
                Spacer()
                
                ShioriLogo()
                    .aspectRatio(contentMode: .fit)
                    .ignoresSafeArea(.keyboard)
                
                Spacer()
                
                VStack {
                    ForEach(viewModel.textFieldConfig) { field in
                        LoginTextField(
                            image: field.image,
                            placeholder: field.placeholder,
                            keyboardType: field.keyboardType,
                            contentType: field.contentType,
                            isSecure: field.isSecure,
                            textFieldValue: getBindingValueFor(field: field.focusedField), 
                            warningTextValue: getWarningTextFor(field: field.focusedField),
                            focused: $focusedField,
                            focusedField: field.focusedField)
                        .padding([.leading, .trailing, .bottom], 8)
                        .onTapGesture {
                            focusedField = field.focusedField
                        }
                    }
                }
                .onSubmit {
                    switch focusedField {
                    case .serverURL:
                        focusedField = .userName
                    case .userName:
                        focusedField = .password
                    case .none:
                        focusedField = .none
                    default:
                        print("Submit \(viewModel.serverURL) - \(viewModel.userName) - \(viewModel.password)")
                    }
                }
                
                Spacer()
                Spacer()
                
                VStack(spacing: 15) {
                    
                    Button {
                        focusedField = .none
                        viewModel.presenter.validateLoginForm()
                    } label: {
                        Text("Login")
                            .font(.title3)
                            .fontWeight(.bold)
                            .frame(minWidth: 180, maxWidth: 300)
                    }
                    .buttonStyle(.borderedProminent)
                    .controlSize(.regular)
                    
                    LoginButtonSeparator()
                    
                    Button {
                        focusedField = .none
                    } label: {
                        Text("Create Account")
                            .font(.body)
                            .fontWeight(.bold)
                            .frame(minWidth: 180, maxWidth: 300)
                            .padding(8)
                            .background(
                                RoundedRectangle(cornerRadius: 10, style: .circular)
                                    .stroke(lineWidth: 1.0)
                            )
                    }
                    .controlSize(.regular)
                }
            }
        }
        .onTapGesture {
            focusedField = .none
        }
    }
    
    private func getBindingValueFor(field: LoginViewModel.Field) -> Binding<String> {
        switch field {
        case .serverURL:
            $viewModel.serverURL
        case .userName:
            $viewModel.userName
        case .password:
            $viewModel.password
        }
    }
    
    private func getWarningTextFor(field: LoginViewModel.Field) -> String {
        switch field {
        case .serverURL:
            viewModel.serverWarning
        case .userName:
            viewModel.usernameWarning
        case .password:
            viewModel.passwordWarning
        }
    }
}


#Preview {
    LoginAssembler.getLoginView()
}

private struct ShioriLogo: View {
    var body: some View {
        Image("Shiori")
            .renderingMode(.original)
            .resizable()
            .frame(minWidth: 110, maxWidth: 130, minHeight: 110, maxHeight: 130)
            .aspectRatio(contentMode: .fit)
        Text("Shiori")
            .font(.largeTitle)
            .fontWeight(.semibold
            )
            .foregroundStyle(.text)
            .aspectRatio(contentMode: .fit)
            .ignoresSafeArea(.keyboard)
    }
}

private struct LoginTextField: View {
    
    let image: Image
    let placeholder: String
    
    let keyboardType: UIKeyboardType
    let contentType: UITextContentType
    
    let isSecure: Bool
    
    @Binding var textFieldValue: String
    var warningTextValue: String
    var focused: FocusState<LoginViewModel.Field?>.Binding
    var focusedField: LoginViewModel.Field
    
    
    var body: some View {
        HStack {
            Spacer()
            VStack {
                HStack {
                    image
                        .imageScale(.large)
                        .foregroundStyle(.primary)
                        .frame(width: 30, height: 30)
                    if isSecure {
                        SecureField(
                            "",
                            text: $textFieldValue,
                            prompt:
                                Text(placeholder)
                                .foregroundColor(.primary))
                        .modifier(LoginTextFieldModifier(keyboardType: keyboardType, contentType: contentType, focused: focused, focusedField: focusedField))
                    } else {
                        TextField(
                            "",
                            text: $textFieldValue,
                            prompt:
                                Text(placeholder)
                                .foregroundColor(.primary))
                        .modifier(LoginTextFieldModifier(keyboardType: keyboardType, contentType: contentType, focused: focused, focusedField: focusedField))
                    }
                }
                .padding(8)
                .background(.textFieldBackground)
                .clipShape(.rect(cornerRadius: 15))
                
                HStack {
                    Spacer()
                    Text(warningTextValue)
                        .font(.caption)
                        .foregroundStyle(.red)
                        .padding(.trailing)
                }
                
            }
            Spacer()
        }
    }
}

private struct LoginButtonSeparator: View {
    var body: some View {
        HStack {
            Spacer()
            Rectangle()
                .foregroundStyle(.secondaryText)
                .frame(width: 100, height: 0.5)
            Text("OR")
                .font(.caption)
                .fontWeight(.bold)
                .foregroundStyle(.secondaryText)
            Rectangle()
                .foregroundStyle(.secondaryText)
                .frame(width: 100, height: 0.5)
            Spacer()
        }
    }
}

private struct LoginTextFieldModifier: ViewModifier {
    
    let keyboardType: UIKeyboardType
    let contentType: UITextContentType
    
    var focused: FocusState<LoginViewModel.Field?>.Binding
    var focusedField: LoginViewModel.Field
    
    func body(content: Content) -> some View {
        content
            .focused(focused, equals: focusedField)
            .keyboardType(keyboardType)
            .textContentType(contentType)
            .textInputAutocapitalization(.never)
            .autocorrectionDisabled()
            .font(.body)
            .frame(maxWidth: 300)
    }
}
