//
//  LoginPresenter.swift
//  Shiori
//
//  Created by Abilash S on 26/02/24.
//

import Foundation

class LoginPresenter: LoginPresenterContract {
    
    weak var viewModel: LoginViewModel!
    
    func validateLoginForm() {
        guard isLoginFieldsEmpty() else { return }
    }
    
}

extension LoginPresenter {
    
    private func isLoginFieldsEmpty() -> Bool {
        var isEmpty = true
        if viewModel.serverURL.isEmpty {
            viewModel.serverWarning = "URL can't be empty"
            isEmpty = false
        }
        if viewModel.userName.isEmpty {
            viewModel.usernameWarning = "User name can't be empty"
            isEmpty = false
        }
        if viewModel.password.isEmpty {
            viewModel.passwordWarning = "Password can't be empty"
            isEmpty = false
        }
        return isEmpty
    }
    
}
