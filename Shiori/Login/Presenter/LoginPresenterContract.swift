//
//  LoginPresenterContract.swift
//  Shiori
//
//  Created by Abilash S on 26/02/24.
//

import Foundation

protocol LoginPresenterContract {
    
    var viewModel: LoginViewModel! { get set }
    
    func validateLoginForm() 
    
}
