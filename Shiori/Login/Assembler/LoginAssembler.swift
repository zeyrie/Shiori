//
//  LoginAssembler.swift
//  Shiori
//
//  Created by Abilash S on 25/02/24.
//

import Foundation

struct LoginAssembler {
    
    static func getLoginView() -> LoginPage {
        let presenter = LoginPresenter()
        let viewModel = LoginViewModel(presenter: presenter)
        presenter.viewModel = viewModel
        let login = LoginPage(viewModel: viewModel)
        return login
    }
    
}
