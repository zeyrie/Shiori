//
//  ShioriApp.swift
//  Shiori
//
//  Created by Abilash S on 24/02/24.
//

import SwiftUI

@main
struct ShioriApp: App {
    var body: some Scene {
        WindowGroup {
            LoginAssembler.getLoginView()
        }
    }
}
